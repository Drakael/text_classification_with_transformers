Simple pipeline for text classification using Transformers


Training done with strafified Kfolds, default is 2 for testing, 5 is recommended

https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.StratifiedKFold.html

Ensure to set batch_size according to your hardware



Availables models : see https://huggingface.co/transformers/pretrained_models.html



Models for French: (flaubert-base-uncased recommended)

flaubert - flaubert-small-cased

flaubert - flaubert-base-uncased

flaubert - flaubert-base-cased

flaubert - flaubert-large-cased

         
camembert - camembert-base



Models for Spanish:

bert - dccuchile/bert-base-spanish-wwm-uncased

bert - dccuchile/bert-base-spanish-wwm-cased



Models for Portuguese:

bert - neuralmind/bert-base-portuguese-cased

bert - neuralmind/bert-large-portuguese-cased