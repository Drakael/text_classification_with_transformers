# coding: utf-8
import argparse
import pandas as pd
from simpletransformers.classification import ClassificationModel
import os
import json
from time import time
import math
import numpy as np


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-batch', type=str, default='atis_test.tsv',
                        help="Batch to infer on (string list)")
    parser.add_argument('-architecture', type=str, default='albert',
                        help="Architecture of the model (bert, roberta, distilbert ...)")
    parser.add_argument('-model', type=str, default='albert-base-v1',
                        help="Model name (bert-base-cased, albert-base-v1 ...)")
    parser.add_argument('-batch_size', type=int, default=64)
    parser.add_argument('-lowercase', type=str2bool, const=True, default=False,
                        nargs='?', help="Perform lowercase on batch.")
    parser.add_argument('-max_length', type=int, default=128,
                        help="Max string length")
    parser.add_argument('-gpu', type=str2bool, const=True, default=True,
                        nargs='?', help="Use GPU and CUDA")
    args = parser.parse_args()

    classes_file = args.architecture+'_'+args.model+'_classes.json'
    classes_data = None
    class_list = None
    class2id = None
    id2class = None
    if os.path.exists(classes_file):
        with open(classes_file, 'r', encoding='utf8') as f:
            classes_data = json.load(f)
            class_list = classes_data['class_list']
            class2id = classes_data['class2id']
            id2class = classes_data['id2class']
    else:
        print('No classes data found at '+classes_file)
    if classes_data is not None:

        # Create a ClassificationModel
        t0 = time()
        save_folder = './trained_'+args.architecture+'_'+args.model+'/'
        print('saved model at', save_folder)
        model = ClassificationModel(args.architecture, save_folder,
                                    num_labels=len(class_list), use_cuda=args.gpu,
                                    args={"do_lower_case": args.lowercase,
                                          'reprocess_input_data': True,
                                          'overwrite_output_dir': True,
                                          "train_batch_size": args.batch_size,
                                          "eval_batch_size": args.batch_size,
                                          "max_seq_length": args.max_length,
                                          "fp16": False,
                                          "evaluate_during_training": True,
                                          "use_early_stopping": False,
                                          "evaluate_during_training_verbose": True,
                                          "save_eval_checkpoints": False,
                                          "save_model_every_epoch": False,
                                          "tensorboard_dir": None})
        print('Model loading done in {}s'.format(time() - t0))

        # Predict
        all_predictions = None
        t0 = time()
        batch = args.batch
        if type(batch) is list and len(batch) > 0 and type(batch[0]) is str:
            df_batch = pd.DataFrame(batch, columns =['query'])
        if type(batch) is str and os.path.exists(batch):
            df_batch = pd.read_csv(batch, sep='\t', names=['intent', 'query'])
            df_batch['intent'] = df_batch['intent'].map(class2id)
            df_batch = df_batch[['query', 'intent']]
            df_batch.dropna(inplace=True)
        if len(df_batch) > 0:
            for i in range(math.ceil(len(df_batch) / args.batch_size)):
                start = i * args.batch_size
                end = min(start + args.batch_size, len(df_batch))
                batch = df_batch['query'][start:end].values.tolist()
                predictions, raw_outputs = model.predict(batch)
                # print(i, 'predictions', predictions, type(predictions))
                # print('raw_outputs', raw_outputs)
                if all_predictions is None:
                    all_predictions = predictions
                else:
                    all_predictions = np.concatenate((all_predictions, predictions), axis=0)
            if 'intent' in df_batch.columns:
                gold = df_batch['intent'].values.astype('int64')
                accuracy = np.sum(gold == all_predictions) / len(gold)
                print('Test accuracy :', accuracy)
            print('Inference done in {}s on {} examples'.format(time() - t0, len(all_predictions)))
