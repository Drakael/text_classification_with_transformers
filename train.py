# coding: utf-8
import argparse
import pandas as pd
from simpletransformers.classification import ClassificationModel
import json
from sklearn.model_selection import StratifiedKFold
import os
import shutil

def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-src_train', type=str, default='atis_train.tsv')
    parser.add_argument('-architecture', type=str, default='albert',
                        help="Architecture of the model (bert, roberta, distilbert ...)")
    parser.add_argument('-model', type=str, default='albert-base-v1',
                        help="Model name (bert-base-cased, albert-base-v1 ...)")
    parser.add_argument('-batch_size', type=int, default=8)
    parser.add_argument('-epochs', type=int, default=16,
                        help="Number of training cycles per training")
    parser.add_argument('-lowercase', type=str2bool, const=True, default=False,
                        nargs='?', help="Perform lowercase on batch.")
    parser.add_argument('-max_length', type=int, default=128,
                        help="Max string length")
    parser.add_argument('-kfolds', type=int, default=2,
                        help="Number of Kfolds for cross validation")
    parser.add_argument('-gpu', type=str2bool, const=True, default=True,
                        nargs='?', help="Use GPU and CUDA")
    args = parser.parse_args()

    train_file = args.src_train

    if os.path.exists(train_file):
        df_train = pd.read_csv(train_file, sep='\t', names=['intent', 'query'])
    else:
        print('src_train file not found at', train_file)
        quit()

    class_list = df_train['intent'].unique().tolist()
    id2class = {k: v for k, v in enumerate(class_list)}
    class2id = {v: k for k, v in id2class.items()}
    classes_data = {
        'class_list': class_list,
        'id2class': id2class,
        'class2id': class2id
    }
    with open(args.architecture+'_'+args.model+'_classes.json', 'w', encoding='utf8') as f:
        json.dump(classes_data, f)

    df_train['intent'] = df_train['intent'].map(class2id)
    df_train = df_train[['query', 'intent']]
    df_train.dropna(inplace=True)

    X = df_train['query'].values
    y = df_train['intent'].values

    skf = StratifiedKFold(n_splits=args.kfolds, random_state=None,
                          shuffle=True)
    skf.get_n_splits(X, y)

    index = 0
    while os.path.exists('training_'+str(index)):
        index += 1
    training_dir = 'training_'+str(index)
    os.mkdir(training_dir)

    fold = 0
    folds_results = list()
    best_loss = 1e9
    best_accuracy = 0.0
    best_loss_fold = -1
    best_accuracy_fold = -1
    for train_index, test_index in skf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        sub_df_train = pd.DataFrame(list(zip(X_train, y_train)),
                                    columns=['query', 'intent'])
        sub_df_test = pd.DataFrame(list(zip(X_test, y_test)),
                                   columns=['query', 'intent'])
        best_model_dir = os.path.join(training_dir, 'best_model_'+str(fold))
        # Create a ClassificationModel
        model = ClassificationModel(args.architecture, args.model,
                                    num_labels=len(class_list), use_cuda=args.gpu,
                                    args={"do_lower_case": args.lowercase,
                                          'num_train_epochs': args.epochs,
                                          'reprocess_input_data': True,
                                          'overwrite_output_dir': True,
                                          "train_batch_size": args.batch_size,
                                          "eval_batch_size": args.batch_size,
                                          "max_seq_length": args.max_length,
                                          "fp16": False,
                                          "evaluate_during_training": True,
                                          "use_early_stopping": False,
                                          "evaluate_during_training_verbose": True,
                                          "save_eval_checkpoints": False,
                                          "save_model_every_epoch": False,
                                          "tensorboard_dir": None,
                                          "best_model_dir": best_model_dir})
        # Train the model
        model.train_model(sub_df_train, eval_df=sub_df_test)
        with open(os.path.join(best_model_dir, 'eval_results.txt'), 'r', encoding='utf8') as f:
            results = f.read().strip().split('\n')
        loss = float(results[0].split(' = ')[1])
        accuracy = float(results[1].split(' = ')[1])
        if loss < best_loss:
            best_loss = loss
            best_loss_fold = fold
        if accuracy > best_accuracy:
            best_accuracy = accuracy
            best_accuracy_fold = fold
        folds_results.append((loss, accuracy))
        fold += 1
    save_folder = 'trained_'+args.architecture+'_'+args.model
    if not os.path.exists(save_folder):
        os.mkdir(save_folder)

    best_model_folder = os.path.join(training_dir, 'best_model_'+str(best_loss_fold))

    def move_file(file):
        file_path = os.path.join(best_model_folder, file)
        new_path = os.path.join(save_folder, file)
        if os.path.exists(new_path):
            os.remove(new_path)
        os.rename(file_path, new_path)

    move_file('pytorch_model.bin')
    move_file('eval_results.txt')
    move_file('config.json')
    move_file('spiece.model')
    move_file('special_tokens_map.json')
    move_file('tokenizer_config.json')

    shutil.rmtree(training_dir)
    shutil.rmtree('cache_dir')
    shutil.rmtree('outputs')
